#ifndef CUSTOM_LIBS_NRF905_SPI_H_
#define CUSTOM_LIBS_NRF905_SPI_H_

#include <msp430.h>
#include "commonlibs.h"
#include <stdio.h>
#include <stdint.h>
#include "nrf905_timer.h"
#define tempMacro        0x16

extern char MST_Data, SLV_Data;

void spi_init(void);
//__interrupt void SPI0_rx (void);

inline void spi_transfer_nr(uint8_t data)
{
    TXBUF1 = data;
    uint32_t start123 = x1;
//    while (((IFG1 & UTXIFG0) == 0));
    while (((IFG2 & UTXIFG1) == 0) && (x1 - start123 > 50));
}

inline uint8_t spi_transfer(uint8_t data)
{
    TXBUF1 = data;
    uint32_t start123 = x1;
//    while (((IFG1 & UTXIFG0) == 0));
//    while (((IFG1 & URXIFG0) == 0));
    while (((IFG2 & URXIFG1) == 0) && (x1 - start123 > 50));
    return RXBUF1;
}

#endif /* CUSTOM_LIBS_NRF905_SPI_H_ */
