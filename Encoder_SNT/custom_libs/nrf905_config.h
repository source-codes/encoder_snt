#ifndef CUSTOM_LIBS_NRF905_CONFIG_H_
#define CUSTOM_LIBS_NRF905_CONFIG_H_

// Crystal frequency (the one the radio module is using)
// NRF905_CLK_4MHZ
// NRF905_CLK_8MHZ
// NRF905_CLK_12MHZ
// NRF905_CLK_16MHZ
// NRF905_CLK_20MHZ
#define NRF905_CLK_FREQ     NRF905_CLK_16MHZ

// Use pin interrupt for data ready (DR)
// NOTE: If you have other devices connected that use the SPI bus then you will need to wrap those bits of code in NRF905_NO_INTERRUPT() blocks
// If NRF905_INTERRUPTS is 0 then you will need to call nRF905_SERVICE() as often as possible
#define NRF905_INTERRUPTS   0

// If you want to use the NRF905_CB_ADDRMATCH and NRF905_CB_RXINVALID callbacks with interrupts then both NRF905_INTERRUPTS and NRF905_INTERRUPTS_AM need to be enabled
// If using NRF905_INTERRUPTS_AM then the timing of when the DR and AM interrupts fire is important. Don't let an interrupt run or disable interrupts for longer than it takes to receive a packet from beginning to end (thats around 7ms with default settings)
// If an interrupt takes longer than 7ms or interrupts are disabled for longer than 7ms then you will need to set NRF905_INTERRUPTS_AM to 0, otherwise received packets will always be marked as invalid.
#define NRF905_INTERRUPTS_AM    0

// If other libraries communicate with SPI devices while inside an interrupt then set this to 1, otherwise you can set this to 0
// If you're not sure then leave this at 1
// If this is 1 then global interrupts will be turned off when this library uses the SPI bus
#define NRF905_INT_SPI_COMMS    0

// Use software to get data ready state instead of reading pin for high/low state, this means you don't need to connect the DR pin
// This option can only be used if NRF905_INTERRUPTS is 0
#define NRF905_DR_SW        0

// Use software to get address match state instead of reading pin for high/low state, this means you don't need to connect the AM pin
// This option can only be used if NRF905_INTERRUPTS_AM is 0
#define NRF905_AM_SW        0

// Don't transmit if airway is busy (other transmissions are going on)
// This feature uses the CD pin
#define NRF905_COLLISION_AVOID  0

///////////////////
// Default radio settings
///////////////////

// Frequency
// Channel 0 is 422.4MHz for the 433MHz band, each channel increments the frequency by 100KHz, so channel 10 would be 423.4MHz
// Channel 0 is 844.8MHz for the 868/915MHz band, each channel increments the frequency by 200KHz, so channel 10 would be 846.8MHz
// Max channel is 511 (473.5MHz / 947.0MHz)
#define NRF905_CHANNEL          10

// Frequency band
// 868 and 915 are actually the same thing
// NRF905_BAND_433
// NRF905_BAND_868
// NRF905_BAND_915
#define NRF905_BAND         NRF905_BAND_433

// Output power
// n means negative, n10 = -10
// NRF905_PWR_n10 (-10dBm = 100uW)
// NRF905_PWR_n2 (-2dBm = 631uW)
// NRF905_PWR_6 (6dBm = 4mW)
// NRF905_PWR_10 (10dBm = 10mW)
#define NRF905_PWR          NRF905_PWR_10

// Save a few mA by reducing receive sensitivity
// NRF905_LOW_RX_DISABLE (Normal sensitivity)
// NRF905_LOW_RX_ENABLE (Lower sensitivity)
#define NRF905_LOW_RX       NRF905_LOW_RX_DISABLE

// Constantly retransmit payload while in transmit mode
// Can be useful in areas with lots of interference, but you'll need to make sure you can differentiate between re-transmitted packets and new packets (like an ID number).
// It will also block other transmissions if collision avoidance is enabled.
// NRF905_AUTO_RETRAN_DISABLE
// NRF905_AUTO_RETRAN_ENABLE
#define NRF905_AUTO_RETRAN  NRF905_AUTO_RETRAN_DISABLE

// Output a clock signal on pin 3 of IC
// NRF905_OUTCLK_DISABLE
// NRF905_OUTCLK_500KHZ
// NRF905_OUTCLK_1MHZ
// NRF905_OUTCLK_2MHZ
// NRF905_OUTCLK_4MHZ
#define NRF905_OUTCLK       NRF905_OUTCLK_DISABLE

// CRC checksum
// NRF905_CRC_DISABLE
// NRF905_CRC_8
// NRF905_CRC_16
#define NRF905_CRC          NRF905_CRC_16

// Address size
// The address is actually the SYNC part of the packet, just after the preamble and before the data
// NRF905_ADDR_SIZE_1 (not recommended, a lot of false invalid packets will be received)
// NRF905_ADDR_SIZE_4
#define NRF905_ADDR_SIZE    NRF905_ADDR_SIZE_4

// Payload size (1 - 32)
#define NRF905_PAYLOAD_SIZE  NRF905_MAX_PAYLOAD


///////////////////
// Interrupt register stuff
// Only needed if NRF905_INTERRUPTS or NRF905_INTERRUPTS_AM are 1
///////////////////

// Interrupt number (INT0, INT1 etc)
// This must match the INT that the DR pin is connected to
#define NRF905_INTERRUPT_NUM_DR 1

// Interrupt number
// This must match the INT that the AM pin is connected to
#define NRF905_INTERRUPT_NUM_AM 0


#define ENCODER_board_selected              1
#define DECODER_board_selected              (!(ENCODER_board_selected))




//Modify for MSP430. Below Interrupts are for AVR Controllers
// Leave these commented out to let the library figure out what registers to use

// Which interrupt to use for data ready (DR)
//#define NRF905_REG_EXTERNAL_INT_DR    EIMSK
//#define NRF905_BIT_EXTERNAL_INT_DR    INT1
//#define NRF905_INT_VECTOR_DR      INT1_vect

// Set interrupt to trigger on rising edge
//#define NRF905_REG_EXTERNAL_INT_CTL_DR    EICRA
//#define NRF905_BIT_EXTERNAL_INT_CTL_DR    (_BV(ISC11)|_BV(ISC10)) // Rising


// Which interrupt to use for address match (AM)
//#define NRF905_REG_EXTERNAL_INT_AM    EIMSK
//#define NRF905_BIT_EXTERNAL_INT_AM    INT0
//#define NRF905_INT_VECTOR_AM      INT0_vect

// Set interrupt to trigger on any change
//#define NRF905_REG_EXTERNAL_INT_CTL_AM    EICRA
//#define NRF905_BIT_EXTERNAL_INT_CTL_AM    (_BV(ISC00)) // Any change


#endif /* CUSTOM_LIBS_NRF905_CONFIG_H_ */
