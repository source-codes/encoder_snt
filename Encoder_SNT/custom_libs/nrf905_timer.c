#include "nrf905_timer.h"

volatile uint32_t x1 = 0;

void timer_init(void)
{
    CCTL0 = CCIE;                             // CCR0 interrupt enabled
    CCR0 = 50000;
    TACTL = TASSEL_2 + MC_1;                  // SMCLK, upmode

    __bis_SR_register(GIE);       // Enter LPM0 w/ interrupt
}

void delay_msec(uint32_t val)
{
    uint32_t temp1 = x1;
    while(1)
    {
        if((x1 - temp1) > (val - 1))
        {
            break;
        }
    }
}


// Timer A0 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMERA0_VECTOR))) Timer_A (void)
#else
#error Compiler not supported!
#endif
{
    x1++;
}
