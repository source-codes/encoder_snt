#include "nrf905_spi.h"

char MST_Data = tempMacro, SLV_Data = 0xFF;
int k=0;

void spi_init(void)
{
    P5SEL = 0x00E;                            // Setup P3 for SPI mode
    P5OUT = 0x001;                            // Setup P3.0 for slave initialization
    P5DIR |= 0x001;
    U1CTL = CHAR + SYNC + MM + SWRST;         // 8-bit, SPI, Master
    U1TCTL =  SSEL1 + STC;              // Polarity, SMCLK, 3-wire
    U1BR0 = 0x002;                            // SPICLK = SMCLK/2
    U1BR1 = 0x000;
    U1MCTL = 0x000;
    ME2 = USPIE1;                             // Module enable
    U1CTL &= ~SWRST;                          // SPI enable
//    IE1 |= URXIE0;                            // Recieve interrupt enable
//    __enable_interrupt();                     // Enable interrupts

    P5OUT &= ~0x001;                          // Toggle P3.0: slave reset
    P5OUT |= 0x001;
}

//#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
//#pragma vector=USART0RX_VECTOR
//__interrupt void SPI0_rx (void)
//#elif defined(__GNUC__)
//void __attribute__ ((interrupt(USART0RX_VECTOR))) SPI0_rx (void)
//#else
//#error Compiler not supported!
//#endif
//{
//  P3OUT ^= 0x010;                           // XOR P3.4 for scope trigger
//  while ((IFG1 & UTXIFG0) == 0);            // USART0 TX buffer ready?
//  if (U0RXBUF == SLV_Data)                  // Test for correct character RX'd
//  {
//    SLV_Data = SLV_Data -1;                 // Decrement incoming data mask
//    MST_Data = MST_Data +1;                 // Increment out-going data
//    TXBUF0 = MST_Data;
//    P1OUT |= 0x001;                         // Pulse P1.0 indicating valid data
//    P1OUT &= ~0x001;
//  }
//  else
//  {
//      if(k==0)
//      {
//          k=1;
//          MST_Data = 0;
//      }
//      else if(k==1)
//      {
//          k=0;
//          MST_Data = tempMacro;
//      }
//    TXBUF0 = MST_Data;
//    P1OUT |= 0x001;                         // Set P1.0 indicating data error
//  }
//}

