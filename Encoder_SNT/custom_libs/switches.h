#ifndef CUSTOM_LIBS_SWITCHES_H_
#define CUSTOM_LIBS_SWITCHES_H_

#include <msp430.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define SwitchesConfigured      22

#define SW01_IOdir      P6DIR
#define SW01_port       P6IN
#define SW01_pin        BIT3
#define SW01_offset     3

#define SW02_IOdir      P6DIR
#define SW02_port       P6IN
#define SW02_pin        BIT4
#define SW02_offset     4

#define SW03_IOdir      P6DIR
#define SW03_port       P6IN
#define SW03_pin        BIT5
#define SW03_offset     5

#define SW04_IOdir      P6DIR
#define SW04_port       P6IN
#define SW04_pin        BIT6
#define SW04_offset     6

#define SW05_IOdir      P6DIR
#define SW05_port       P6IN
#define SW05_pin        BIT7
#define SW05_offset     7

#define SW06_IOdir      P2DIR
#define SW06_port       P2IN
#define SW06_pin        BIT0
#define SW06_offset     0

#define SW07_IOdir      P2DIR
#define SW07_port       P2IN
#define SW07_pin        BIT1
#define SW07_offset     1

#define SW08_IOdir      P2DIR
#define SW08_port       P2IN
#define SW08_pin        BIT3
#define SW08_offset     3

#define SW09_IOdir      P2DIR
#define SW09_port       P2IN
#define SW09_pin        BIT4
#define SW09_offset     4

#define SW10_IOdir      P2DIR
#define SW10_port       P2IN
#define SW10_pin        BIT5
#define SW10_offset     5

#define SW11_IOdir      P2DIR
#define SW11_port       P2IN
#define SW11_pin        BIT6
#define SW11_offset     6

#define SW12_IOdir      P2DIR
#define SW12_port       P2IN
#define SW12_pin        BIT7
#define SW12_offset     7

#define SW13_IOdir      P3DIR
#define SW13_port       P3IN
#define SW13_pin        BIT4
#define SW13_offset     4

#define SW14_IOdir      P3DIR
#define SW14_port       P3IN
#define SW14_pin        BIT5
#define SW14_offset     5

#define SW15_IOdir      P3DIR
#define SW15_port       P3IN
#define SW15_pin        BIT6
#define SW15_offset     6

#define SW16_IOdir      P3DIR
#define SW16_port       P3IN
#define SW16_pin        BIT7
#define SW16_offset     7

#define SW17_IOdir      P4DIR
#define SW17_port       P4IN
#define SW17_pin        BIT0
#define SW17_offset     0

#define SW18_IOdir      P4DIR
#define SW18_port       P4IN
#define SW18_pin        BIT1
#define SW18_offset     1

#define SW19_IOdir      P4DIR
#define SW19_port       P4IN
#define SW19_pin        BIT2
#define SW19_offset     2

#define SW20_IOdir      P4DIR
#define SW20_port       P4IN
#define SW20_pin        BIT3
#define SW20_offset     3

#define SW21_IOdir      P4DIR
#define SW21_port       P4IN
#define SW21_pin        BIT4
#define SW21_offset     4

#define SW22_IOdir      P4DIR
#define SW22_port       P4IN
#define SW22_pin        BIT5
#define SW22_offset     5




extern uint8_t switchstatus_array[SwitchesConfigured];

void switches_init(void);
uint32_t readSwitchStatus(void);













#endif /* CUSTOM_LIBS_SWITCHES_H_ */
