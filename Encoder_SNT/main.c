#include <msp430.h> 
#include "nrf905.h"
#include "nrf905_timer.h"
#include "switches.h"
#define RXADDR 0xE7E7E7E7 // Address of this device
#define TXADDR 0xE7E7E7E7 // Address of device to send to

#define TestSequenceNum 2
#define PACKET_NONE     0
#define PACKET_OK       1
#define PACKET_INVALID  2

static volatile uint32_t milliseconds;

static volatile uint8_t packetStatus;

uint32_t SwitchStatus = 0;


void NRF905_CB_RXCOMPLETE(void)
{
    packetStatus = PACKET_OK;
    nRF905_standby();
}

void NRF905_CB_RXINVALID(void)
{
    packetStatus = PACKET_INVALID;
    nRF905_standby();
}

uint32_t millis(void)
{
    uint32_t ms;
    {
        ms = milliseconds;
    }
    return ms;
}

int main(void)
{
    DCOCTL = DCO0 + DCO1 + DCO2;              // Max DCO
    BCSCTL1 = RSEL0 + RSEL1 + RSEL2;
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
    P1DIR |= BIT0;                  // Initialize LED at P1.0
    P1DIR |= (BIT5 + BIT6 + BIT7);                            // Set P1.0 to output direction
    switches_init();
    timer_init();

    delay_msec(5);

    // Start up
    nRF905_init();

    // Set address of this device
    nRF905_setListenAddress(RXADDR);

    uint8_t counter = 0;
    uint32_t sent = 0;
    uint32_t replies = 0;
    uint32_t timeouts = 0;
    uint32_t invalids = 0;

    P1OUT |= 0x01;
    uint32_t switchtimeout = x1;
    SwitchStatus = readSwitchStatus();

    while (1)
    {
        // Make data
//        sprintf_P(data, PSTR("test %hhu"), counter);
        uint8_t data[NRF905_MAX_PAYLOAD];
        counter++;
//        P1OUT ^= (BIT5 + BIT6 + BIT7);                          // Toggle P1.0 using exclusive-OR


        packetStatus = PACKET_NONE;

//        if(x1 - switchtimeout > 10)
//        {
            SwitchStatus = readSwitchStatus();
            switchtimeout = x1;
            data[0] = 0x00;
            data[1] = 0xFF;
            data[2] = 0x00;
            data[3] = 0xFF;
            data[4] = 0x00;

            if(switchstatus_array[20] == 1)
            {
                data[2] = 0xFF;
                P1OUT |= BIT7;
            }
            else if(switchstatus_array[20] == 0)
            {
                P1OUT &= ~BIT7;
            }
            if(switchstatus_array[21] == 1)
            {
                data[4] = 0xFF;
                P1OUT |= BIT6;
            }
            else if(switchstatus_array[21] == 0)
            {
                P1OUT &= ~BIT6;
            }
//        }

//        uint8_t k=0;
//        for(k=0; k<SwitchesConfigured; k++)
//        {
//            data[k] = switchstatus_array[k];
//        }
//        for(k=SwitchesConfigured; k<NRF905_MAX_PAYLOAD; k++)
//        {
//            data[k] = 99;
//        }
//        printf_P(PSTR("Sending data: %s\n"), data);

//        uint32_t startTime = x1;

        // Send the data (send fails if other transmissions are going on, keep trying until success) and enter RX mode on completion
        while(!nRF905_TX(TXADDR, (uint8_t*)data, NRF905_MAX_PAYLOAD, NRF905_NEXTMODE_STANDBY));
        sent++;
//        }
//        puts_P(PSTR("Data sent, waiting for reply..."));

//        uint8_t success;
//
//        // Wait for reply with timeout
//        uint32_t sendStartTime = x1;
//        while(1)
//        {
//            success = packetStatus;
//            if(success != PACKET_NONE)
//                break;
//            else if(x1 - sendStartTime > TIMEOUT)
//                break;
//        }
//
//        if(success == PACKET_NONE)
//        {
////            puts_P(PSTR("Ping timed out"));
//            timeouts++;
//        }
//        else if(success == PACKET_INVALID)
//        {
//            // Got a corrupted packet
////            puts_P(PSTR("Invalid packet!"));
//            invalids++;
//        }
//        else
//        {
//            // If success toggle LED and send ping time over UART
////            uint16_t totalTime = millis() - startTime;
//            P1OUT ^= 0x01;                          // Toggle P1.0 using exclusive-OR
//
//            replies++;
//
////            printf_P(PSTR("Ping time: %ums\n"), totalTime);
//
//            // Get the ping data
//            uint8_t replyData[NRF905_MAX_PAYLOAD];
//            nRF905_read(replyData, sizeof(replyData));
//
//            // Print out ping contents
////            printf_P(PSTR("Data from server: "));
////            for(uint8_t i=0;i<sizeof(replyData);i++)
////                printf_P(PSTR("%c"), replyData[i]);
////            puts_P(PSTR(""));
//        }
//
////        printf_P(PSTR("Totals: %lu Sent, %lu Replies, %lu Timeouts, %lu Invalid\n------\n"), sent, replies, timeouts, invalids);
//
//         delay_msec(5);
//         P1OUT ^= 0x01;
    }
}



