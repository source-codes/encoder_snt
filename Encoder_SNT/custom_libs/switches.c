#include "switches.h"

uint8_t switchstatus_array[SwitchesConfigured] = {0};

void switches_init(void)
{
    //Switches configured on

    //switches
    P6DIR &= ~(BIT3 + BIT4 + BIT5 + BIT6 + BIT7);
    P2DIR &= ~(BIT0 + BIT1 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7);
    P3DIR &= ~(BIT4 + BIT5 + BIT6 + BIT7);
    P4DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7);

//    SW01_IOdir  &= ~SW01_pin;           //1 - 6.3
//    SW02_IOdir  &= ~SW02_pin;           //2 - 6.4
//    SW03_IOdir  &= ~SW03_pin;           //3 - 6.5
//    SW04_IOdir  &= ~SW04_pin;           //4 - 6.6
//    SW05_IOdir  &= ~SW05_pin;           //5 - 6.7
//    SW06_IOdir  &= ~SW06_pin;           //6 - 2.0
//    SW07_IOdir  &= ~SW07_pin;           //7 - 2.1
//    SW08_IOdir  &= ~SW08_pin;           //8 - 2.3
//    SW09_IOdir  &= ~SW09_pin;           //9 - 2.4
//    SW10_IOdir  &= ~SW10_pin;           //10 - 2.5
//    SW11_IOdir  &= ~SW11_pin;           //11 - 2.6
//    SW12_IOdir  &= ~SW12_pin;           //12 - 2.7
//    SW13_IOdir  &= ~SW13_pin;           //13 - 3.4
//    SW14_IOdir  &= ~SW14_pin;           //14 - 3.5
//    SW15_IOdir  &= ~SW15_pin;           //15 - 3.6
//    SW16_IOdir  &= ~SW16_pin;           //16 - 3.7
//    SW17_IOdir  &= ~SW17_pin;           //17 - 4.0
//    SW18_IOdir  &= ~SW18_pin;           //18 - 4.1
//    SW19_IOdir  &= ~SW19_pin;           //19 - 4.2
//    SW20_IOdir  &= ~SW20_pin;           //20 - 4.3
//    SW21_IOdir  &= ~SW21_pin;           //21 - 4.4
//    SW22_IOdir  &= ~SW22_pin;           //22 - 4.5
}

uint32_t readSwitchStatus(void)
{
    uint32_t tempSwitchStatus = 0;

//    memset(switchstatus_array, 0, sizeof(switchstatus_array));

    switchstatus_array[0] = (SW01_port & SW01_pin) >> SW01_offset;
    switchstatus_array[1] = (SW02_port & SW02_pin) >> SW02_offset;
    switchstatus_array[2] = (SW03_port & SW03_pin) >> SW03_offset;
    switchstatus_array[3] = (SW04_port & SW04_pin) >> SW04_offset;
    switchstatus_array[4] = (SW05_port & SW05_pin) >> SW05_offset;
    switchstatus_array[5] = (SW06_port & SW06_pin) >> SW06_offset;
    switchstatus_array[6] = (SW07_port & SW07_pin) >> SW07_offset;
    switchstatus_array[7] = (SW08_port & SW08_pin) >> SW08_offset;
    switchstatus_array[8] = (SW09_port & SW09_pin) >> SW09_offset;
    switchstatus_array[9] = (SW10_port & SW10_pin) >> SW10_offset;
    switchstatus_array[10] = (SW11_port & SW11_pin) >> SW11_offset;
    switchstatus_array[11] = (SW12_port & SW12_pin) >> SW12_offset;
    switchstatus_array[12] = (SW13_port & SW13_pin) >> SW13_offset;
    switchstatus_array[13] = (SW14_port & SW14_pin) >> SW14_offset;
    switchstatus_array[14] = (SW15_port & SW15_pin) >> SW15_offset;
    switchstatus_array[15] = (SW16_port & SW16_pin) >> SW16_offset;
    switchstatus_array[16] = (SW17_port & SW17_pin) >> SW17_offset;
    switchstatus_array[17] = (SW18_port & SW18_pin) >> SW18_offset;
    switchstatus_array[18] = (SW19_port & SW19_pin) >> SW19_offset;
    switchstatus_array[19] = (SW20_port & SW20_pin) >> SW20_offset;
    switchstatus_array[20] = (SW21_port & SW21_pin) >> SW21_offset;
    switchstatus_array[21] = (SW22_port & SW22_pin) >> SW22_offset;

//    switchstatus_array[21] = (SW01_port & SW01_pin) >> SW01_offset;
//    switchstatus_array[20] = (SW02_port & SW02_pin) >> SW02_offset;
//    switchstatus_array[19] = (SW03_port & SW03_pin) >> SW03_offset;
//    switchstatus_array[18] = (SW04_port & SW04_pin) >> SW04_offset;
//    switchstatus_array[17] = (SW05_port & SW05_pin) >> SW05_offset;
//    switchstatus_array[16] = (SW06_port & SW06_pin) >> SW06_offset;
//    switchstatus_array[15] = (SW07_port & SW07_pin) >> SW07_offset;
//    switchstatus_array[14] = (SW08_port & SW08_pin) >> SW08_offset;
//    switchstatus_array[13] = (SW09_port & SW09_pin) >> SW09_offset;
//    switchstatus_array[12] = (SW10_port & SW10_pin) >> SW10_offset;
//    switchstatus_array[11] = (SW11_port & SW11_pin) >> SW11_offset;
//    switchstatus_array[10] = (SW12_port & SW12_pin) >> SW12_offset;
//    switchstatus_array[9] = (SW13_port & SW13_pin) >> SW13_offset;
//    switchstatus_array[8] = (SW14_port & SW14_pin) >> SW14_offset;
//    switchstatus_array[7] = (SW15_port & SW15_pin) >> SW15_offset;
//    switchstatus_array[6] = (SW16_port & SW16_pin) >> SW16_offset;
//    switchstatus_array[5] = (SW17_port & SW17_pin) >> SW17_offset;
//    switchstatus_array[4] = (SW18_port & SW18_pin) >> SW18_offset;
//    switchstatus_array[3] = (SW19_port & SW19_pin) >> SW19_offset;
//    switchstatus_array[2] = (SW20_port & SW20_pin) >> SW20_offset;
//    switchstatus_array[1] = (SW21_port & SW21_pin) >> SW21_offset;
//    switchstatus_array[0] = (SW22_port & SW22_pin) >> SW22_offset;

    uint8_t i;
    for(i=0; i<SwitchesConfigured; i++)
    {
        tempSwitchStatus += (uint32_t)((switchstatus_array[i]) << i);
    }

    return tempSwitchStatus;
}
